# -*- coding: utf-8 -*-
"""
Created on Wed Nov 18 10:26:54 2015

@author: 10woodsa
"""
import sys
import pygame
from pygame.locals import *
import random #imports needed modules
import time
from PyQt4 import QtGui

class App: #creates App class
    """
    Starts the Game window.
    Handles running and window settings
    """
    def __init__(self):
        """
        Size of window
        running set to True to it runs
        no display surf
        """
        self._running=True
        self._display_surf=None
        self.size=self.weight, self.height = 640, 400
        
    def on_init(self):
        """
        initiate pygame
        Running True
        sets surface size
        """
        pygame.init()
        self._display_surf=pygame.display.set_mode(self.size, pygame.HWSURFACE | pygame.DOUBLEBUF)
        self._running=True
    def on_cleanup(self):
        pygame.quit() #uninitiates pygame
 
    def on_execute(self):
        """
        stops running
        loops rendering
        """
        if self.on_init()==False:
            self._running=False
 
        while(self._running ):
            for event in pygame.event.get():
                self.on_event(event)
            self.on_loop()
            self.on_render()
        self.on_cleanup()

class Tile(pygame.Surface):
    """
    sets tiles for surface of game map
    """
    _id=0 #sets ID to start at 0
    t=[] #creates a list for the tiles
    def __init__(self, collide, name, texture):
        """
        Sets behavior of tiles:
        texturew
        Collision or not
        name
        can be build upon
        """
        pygame.Surface.__init__(self,(50,50))
        self.ID =Tile._id #sets ID to tiles ID
        Tile._id+=1 #tile ID
        self.name=name
        self.collide=collide
        self.texture=texture
        self.rect=self.texture.get_rect()
        Tile.t.append(self) #appends the tile ID to the tile list 
        
GROUND=Tile(False,"Ground",pygame.image.load('textures/tiles/ground.png'))        #creates a ground tile that is called GROUND cannot be collided and has a texture
WALL=Tile(True,"Wall",pygame.image.load('textures/tiles/wall.png'))            #creates a wall tile than is called WALL can collide and has a texture

TILESIZE=48 #sets the size of each tile
MAPWIDTH=10 #how many tiles x
MAPHEIGHT=10 #how many tiles y

resources=[GROUND, WALL]
tilemap=[[GROUND for h in range(MAPHEIGHT)] for w in range(MAPWIDTH)] #puts grass in all tiles
pygame.init()
DISPLAYSURF=pygame.display.set_mode((MAPWIDTH*TILESIZE, MAPHEIGHT*TILESIZE)) 
            
def MapGeneration():
    """
    Generates map
    for rows and colums
    gets random number between 0, 100 and depending on that number picks a tile to set the block to
    Randomly generated maps
    Will be changed to procedurally generate maps instead - Adds structure to map
    """
    for cl in range(MAPHEIGHT):
        for rw in range(MAPWIDTH):
            randomNumber=random.randint(1,100)
            if randomNumber>=1 and randomNumber<=20:
                tile=WALL
            elif randomNumber>=21 and randomNumber<=100:
                tile=GROUND
            tilemap[cl][rw]=tile
            tilemap[cl][rw].y = cl
            tilemap[cl][rw].x = rw
            print(tilemap[rw][cl].name, tilemap[rw][cl].x,tilemap[rw][cl].y)

class Player(pygame.sprite.Sprite):
    """
    class for player
    """
    def __init__(self, texture, x, y, live):
        """
        controlls texture
        x coordinates on map
        and y coordinates
        """
        super().__init__()
        self.texture=texture
        self.x=x
        self.y=y
        self.live = live
        
PLAYER=Player(pygame.image.load("texture/player/player.png"), 0, 0, True)
        

def event_handle():
    """
    handles all events 
    Quit 
    Movement
    More can be added
    Certain keys for special actions
    mouse movement
    """
    for event in pygame.event.get():
        if PLAYER.live == False:
            pygame.quit()
            sys.quit()
    keys=pygame.key.get_pressed() 
    if (keys[K_d]) and (keys[K_w]) and PLAYER.y < MAPHEIGHT - 1 and  PLAYER.x > 0: #NE
        if tilemap[PLAYER.x - 1][PLAYER.y + 1] is not WALL:
            PLAYER.texture = player_img_Direction_UpRight
            PLAYER.y+=1
            PLAYER.x-=1
            time.sleep(0.1)
    elif (keys[K_a]) and (keys[K_w]) and PLAYER.y > 0 and  PLAYER.x > 0: #NW
        if tilemap[PLAYER.x-1][PLAYER.y-1] is not WALL:
            PLAYER.texture = player_img_Direction_UpLeft
            PLAYER.y-=1
            PLAYER.x-=1
            time.sleep(0.1)
    elif (keys[K_d]) and (keys[K_s]) and PLAYER.y < MAPHEIGHT - 1 and PLAYER.x < MAPWIDTH - 1: #SE
        if tilemap[PLAYER.x+1][PLAYER.y+1] is not WALL:
            PLAYER.texture = player_img_Direction_DownRight
            PLAYER.y+=1
            PLAYER.x+=1
            time.sleep(0.1)
    elif (keys[K_a]) and (keys[K_s]) and PLAYER.y > 0 and PLAYER.x < MAPWIDTH - 1: #SW
        if tilemap[PLAYER.x+1][PLAYER.y-1] is not WALL:
            PLAYER.texture = player_img_Direction_DownLeft
            PLAYER.y-=1
            PLAYER.x+=1 
            time.sleep(0.1)
    elif (keys[K_s]) and PLAYER.y < MAPWIDTH - 1: #E
        if tilemap[PLAYER.x+1][PLAYER.y] is not WALL:
            PLAYER.texture = player_img_Direction_Down
            PLAYER.x+=1
            time.sleep(0.1)
    elif (keys[K_w]) and PLAYER.y > 0: #W
        if tilemap[PLAYER.x-1][PLAYER.y] is not WALL:
            PLAYER.texture = player_img_Direction_Up
            PLAYER.x-=1
            time.sleep(0.1)
    elif (keys[K_d]) and PLAYER.x < MAPHEIGHT - 1: #S
        if tilemap[PLAYER.x][PLAYER.y+1] is not WALL:
            PLAYER.texture = player_img_Direction_Right
            PLAYER.y+=1
            time.sleep(0.1)
    elif (keys[K_a]) and PLAYER.x > 0: #N
        if tilemap[PLAYER.x][PLAYER.y-1] is not WALL:
            PLAYER.texture = player_img_Direction_Left
            PLAYER.y-=1
            time.sleep(0.1)
    if (keys[K_w]) or (keys[K_s]) or (keys[K_a]) or (keys[K_d]):
        print(PLAYER.y, PLAYER.x)
        
def start():
    MapGeneration() #generates map
    while True: 
        """
        Renders and Draws everything
        Updates movement and anything else that changes during runtime
        """
        #get_high_score()
        event_handle()
        for row in range(MAPHEIGHT):
            for column in range(MAPWIDTH):
                pygame.display.set_caption('Game Name')
                pygame.display.set_icon(pygame.image.load('textures/Player/CirclemodelDown.png'))
                DISPLAYSURF.blit(Tile.t[tilemap[row][column].ID].texture, (column*TILESIZE,row*TILESIZE))
                DISPLAYSURF.blit(PLAYER.texture,(PLAYER.y*TILESIZE, PLAYER.x*TILESIZE))
        pygame.display.update()
        #save_high_score()
start()