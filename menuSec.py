# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 10:55:22 2015

@author: 10woodsa
"""
import sys
from PyQt4 import QtGui #imports all the needed modules
from PyQt4 import QtCore

class Game(QtGui.QMainWindow):
    
    def __init__(self):
        """
        Creates window for menu
        sets size and position
        Adds file menu bar
        adds exit option in file menu bar
        """
        super(Game, self).__init__()
        self.setGeometry(8, 30, 350, 400)
        self.setWindowTitle("Game")
        
        self.body = Body(self) 
        self.setCentralWidget(self.body)
        
        ExitAction = QtGui.QAction("&Exit", self)
        ExitAction.setShortcut("Ctrl+Q")
        ExitAction.setStatusTip('Leave The App')
        ExitAction.triggered.connect(self.close_application)
        
        self.statusBar()
        
        mainMenu = self.menuBar()
        fileMenu = mainMenu.addMenu('&File')
        fileMenu.addAction(ExitAction)
        
        self.show() #shows all to finish
        
    def close_application(self):
        """
        Called when application is wanted to be close
        makes sure user wants to exit
        """
        choice = QtGui.QMessageBox.question(self, 'Exit.',
                                            "Are you sure you want to exit?",
                                            QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)                           
        if choice == QtGui.QMessageBox.Yes:
            sys.exit()
        else:
            pass

class Body(QtGui.QWidget):
    """
    creates the main body of the menu screen
    """
    def __init__(self,parent):
        super(Body, self).__init__(parent)
        self.initUI() #starts menu body

    def initUI(self):
        """
        Creates two buttons one for exit and one for play
        Play imports and starts game
        exit quits menu
        """
        playButton = QtGui.QPushButton("PLAY", self)
        playButton.move(90, 250)
        quitButton = QtGui.QPushButton("QUIT", self)
        quitButton.move(180, 250)
        
        quitButton.clicked.connect(Game.close_application(self))
        self.show()

def main():
    """
    Starts all
    Window
    and body
    """
    app = QtGui.QApplication(sys.argv)
    sys.exit(app.exec_())
    
main() #starts Main