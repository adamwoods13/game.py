# -*- coding: utf-8 -*-
"""
Created on Wed Feb  3 16:06:36 2016

@author: `
"""
import sys
import pygame
from pygame.locals import *
import random #imports needed modules
import time

FPS=60#sets fps to 30
Clock=pygame.time.Clock()
downimg = pygame.image.load("textures/Player/player.png")
upimg = pygame.image.load("textures/Player/player_w.png")
rightimg = pygame.image.load("textures/Player/player_d.png")
leftimg = pygame.image.load("textures/Player/player_a.png")
dmgimg = pygame.image.load("textures/Player/player.png")
#defines frequently used images 





class App: #creates App class
    """
    Starts the Game window.
    Handles running and window settings
    """
    o = []
    def __init__(self):
        """
        Size of window
        running set to True to it runs
        no display surf
        """
        self._running=True
        self._display_surf=None
        self.size=self.weight, self.height = 640, 400   
    def on_init(self):
        """
        initiate pygame
        Running True
        sets surface size
        """
        pygame.init()
        self._display_surf=pygame.display.set_mode(self.size, pygame.HWSURFACE | pygame.DOUBLEBUF)
        self._running=True

class Tile(pygame.Surface):
    """
    sets tiles for surface of game map
    """
    tile = 0
    _id=0 #sets ID to start at 0
    t=[] #creates a list for the tiles
    def __init__(self, collide, name, texture):
        """
        Sets behavior of tiles:
        texture
        Collision or not
        name
        can be build upon
        """
        pygame.Surface.__init__(self,(40,40))
        self.ID =Tile._id #sets ID to tiles ID
        Tile._id+=1 #tile ID
        self.name=name
        self.collide=collide
        self.texture=texture
        self.rect=self.texture.get_rect()
        self.x = 0
        self.y = 0
        Tile.t.append(self)
ground=Tile(False,"Ground",pygame.image.load('textures/tiles/ground.png'))
wall=Tile(True,"Wall",pygame.image.load('textures/tiles/wall.png'))
blank=Tile(False, "Blank", pygame.image.load('textures/tiles/blank.png'))
        #creates three different types of tiles one of which can not be walked on.

        
class Mapgen:
    def __init__(self, TILESIZE, MAPWIDTH, MAPHEIGHT, starvelimit, birthlimit, simsteplimit):
        self.TILESIZE = TILESIZE
        self.MAPWIDTH=MAPWIDTH
        self.MAPHEIGHT=MAPHEIGHT
        self.starvelimit=starvelimit
        self.birthlimit=birthlimit
        self.simsteplimit=simsteplimit
        self.livecount=0
    
    def radcheck(self, rw, cl):
        for i in range(-1,2):
            for j in range(-1,2):
                neighbour_x = rw+i
                neighbour_y = cl+j
                if i == 0 and j == 0:
                    pass               
                elif neighbour_x < 0 or neighbour_y < 0 or neighbour_x >= modeOne.MAPWIDTH or neighbour_y >= modeOne.MAPHEIGHT:
                    self.livecount += 1
                    
                elif tilemap[neighbour_y][neighbour_x].ID == 0:
                    self.livecount += 1
                    
    def genstep(self):
        for rw in range(self.MAPHEIGHT):
            for cl in range(self.MAPWIDTH): 
                self.radcheck(rw,cl)
                try:
                    if tilemap[rw][cl].ID == 1:
                        if self.livecount <= self.starvelimit:
                            Tile.tile = ground
                        else:
                            Tile.tile = wall      
                    elif tilemap[rw][cl].ID == 0:
                        if self.livecount >= self.birthlimit:
                            Tile.tile = wall
                        else:
                            Tile.tile = ground      
                    else:
                        newMap[rw][cl].ID = 2
                    newMap[rw][cl] = Tile.tile
                except IndexError:
                    print("IE")
                self.livecount = 0
                
    def radcheck_two(self, rw, cl):
        for i in range(-1,2):
            for j in range(-1,2):
                neighbour_x = rw+i
                neighbour_y = cl+j
                if i == 0 and j == 0:
                    pass               
                elif neighbour_x < 0 or neighbour_y < 0 or neighbour_x >= modeOne.MAPWIDTH or neighbour_y >= modeOne.MAPHEIGHT:
                    self.livecount += 1
                    
                elif newMap[neighbour_y][neighbour_x].ID == 0:
                    self.livecount += 1
                    
    def genstep_two(self):
        for rw in range(self.MAPHEIGHT):
            for cl in range(self.MAPWIDTH):
                self.radcheck_two(rw,cl)
                try:
                    if newMap[rw][cl].ID == 1:
                        if self.livecount <= self.starvelimit:
                            Tile.tile = ground
                        else:
                            Tile.tile = wall
                    elif newMap[rw][cl].ID == 0:
                        if self.livecount >= self.birthlimit:
                            Tile.tile = wall
                        else:
                            Tile.tile = ground      
                    else:
                        newwMap[rw][cl].ID = 2
                    newwMap[rw][cl] = Tile.tile
                except IndexError:
                    print("IE")
                self.livecount = 0
                
    def radcheck_final(self, rw, cl):
        for i in range(-1,2):
            for j in range(-1,2):
                neighbour_x = rw+i
                neighbour_y = cl+j
                if i == 0 and j == 0:
                    pass               
                elif neighbour_x < 0 or neighbour_y < 0 or neighbour_x >= modeOne.MAPWIDTH or neighbour_y >= modeOne.MAPHEIGHT:
                    self.livecount += 1
                    
                elif newwMap[neighbour_y][neighbour_x].ID == 0:
                    self.livecount += 1
                
    def genstep_final(self):
        for rw in range(self.MAPHEIGHT):
            for cl in range(self.MAPWIDTH):
                self.radcheck_final(rw,cl)
                try:
                    if newwMap[rw][cl].ID == 1:
                        if self.livecount <= self.starvelimit:
                            Tile.tile = ground
                        else:
                            Tile.tile = wall
                    elif newwMap[rw][cl].ID == 0:
                        if self.livecount >= self.birthlimit:
                            Tile.tile = wall
                        else:
                            Tile.tile = ground
                    else:
                        finMap[rw][cl].ID = 2
                    finMap[rw][cl] = Tile.tile
                except IndexError:
                    print("IE")
                self.livecount = 0
        
    def mapGen():
        target = open("high_score.txt", "r")
        prescore = target.read()
        if prescore == "0":
            livechance = 450
        else:
            livechance = prescore
        print("Percentage chance cell is alive: ",livechance)
        for rw in range(modeOne.MAPHEIGHT):
            for cl in range(modeOne.MAPWIDTH):
                MRNG= random.randint(0,1000)
                if MRNG >= int(livechance):
                    Tile.tile = ground
                else:
                    Tile.tile = wall
                tilemap[rw][cl]= Tile.tile
        Mapgen.genstep(modeOne)
        Mapgen.genstep_two(modeOne)
        Mapgen.genstep_final(modeOne)
        
#Game Mode :::Tilesize, Map Width, Map Height, Starve Limit, Birth Limit, Step Limit
modeOne = Mapgen(31, 20, 20, 3, 7, 1)        
tilemap=[[blank for rw in range(modeOne.MAPHEIGHT)]for cl in range(modeOne.MAPWIDTH)]
newMap=[[blank for rw in range(modeOne.MAPHEIGHT)]for cl in range(modeOne.MAPWIDTH)]
newwMap=[[blank for rw in range(modeOne.MAPHEIGHT)]for cl in range(modeOne.MAPWIDTH)]
finMap=[[blank for rw in range(modeOne.MAPHEIGHT)]for cl in range(modeOne.MAPWIDTH)]



class Player(pygame.sprite.Sprite):
    hit_list = []
    def __init__(self, texture, hp, score, imtime=30):
        """
        controlls texture
        x coordinates on map
        and y coordinates
        """
        super().__init__()
        self.x=0
        self.y=0
        self.texture=texture
        self.hp=hp
        self.immune=False
        self.imcount=0
        self.imtime=imtime
        self.score=score
        self.tcount=0
        Player.hit_list.append(self)
        App.o.append(self)
    
    def remove(self):
        App.o.remove(self)
        Player.hit_list.remove(self)
    
    def damage():
        PLAYER.hp -= 1
        PLAYER.immune = True
        print(PLAYER.hp)
        if PLAYER.hp <= 0:
            print("Game Over")
            PLAYER.remove()
            sys.exit()
    
    def spawn(self):
        for rw in range(modeOne.MAPHEIGHT):
            for cl in range(modeOne.MAPWIDTH):
                SRNG=random.randint(1,100)
                if finMap[rw][cl].ID == 0 and SRNG < 6:
                    self.x = rw
                    self.y = cl
                    break
  
PLAYER=Player(downimg, 5, 0)


class Enemy(pygame.sprite.Sprite):
    hit_list = []
    def __init__(self, texture, x, y, spcount, tstart, sptime=5):
        super().__init__()
        self.texture = texture
        self.x=0
        self.y=0
        self.Scoring()
        self.spcount=spcount
        self.tcount = 0
        self.tstart=tstart
        self.sptime=sptime
        
    def Scoring(self):
        SRNG = random.randint(60,110)
        Enemy.adscore = SRNG
        
        
    def movement():
        add(ENEMY)
        add(ENEMY2)
        add(ENEMY3)
        ENEMY.tcount +=1
        ENEMY2.tcount +=1
        ENEMY3.tcount +=1
        print(ENEMY.tcount)
        for enemy in Enemy.hit_list:
            if PLAYER.x == enemy.x and PLAYER.y == enemy.y +1:
                down(enemy)
            elif PLAYER.x == enemy.x and PLAYER.y == enemy.y -1:
                up(enemy)
            elif PLAYER.x == enemy.x -1 and PLAYER.y == enemy.y:
                left(enemy)
            elif PLAYER.x == enemy.x +1 and PLAYER.y == enemy.y:
                right(enemy)
            else:
                MRNG = random.randint(1,5)
                if MRNG == 1:
                    up(enemy)
                    enemy.texture = pygame.image.load("textures/enemy/enemy_walk_w.png")
                elif MRNG == 2:
                    down(enemy)
                    enemy.texture = pygame.image.load("textures/enemy/enemy_walk_s.png")
                elif MRNG == 3:
                    left(enemy)
                    enemy.texture = pygame.image.load("textures/enemy/enemy_walk_a.png")
                elif MRNG == 4:
                    right(enemy)
                    enemy.texture = pygame.image.load("textures/enemy/enemy_walk_d.png")
                elif MRNG == 5:
                    pass
    def spawn(self):
        for rw in range(modeOne.MAPHEIGHT):
            for cl in range(modeOne.MAPWIDTH):
                SRNG=random.randint(1,100)
                if finMap[rw][cl].ID == 0 and SRNG < 6:
                    self.x = rw
                    self.y = cl
                    break
                       
    def remove(self):
        print("boop")
        if App.o.count(self) != 0:
            App.o.remove(self)
            Enemy.hit_list.remove(self)
    

ENEMY=Enemy(pygame.image.load("textures/enemy/enemy_walk_a.png"), 2, 3, 0, 2)
ENEMY2=Enemy(pygame.image.load("textures/enemy/enemy_walk_a.png"), 3, 4, 0, 2)
ENEMY3=Enemy(pygame.image.load("textures/enemy/enemy_walk_a.png"), 7, 4, 0, 2)


            
def add(e):
    if e.tcount == e.sptime:
        e.tcount = 0
        if App.o.count(e) != 1:
            Enemy.spawn(e)
            Enemy.Scoring(e)
            Enemy.hit_list.append(e)
            App.o.append(e)
            
class tempMisc(pygame.sprite.Sprite):
    o = []
    def __init__(self,name, texture, x, y,Ttime=25):
        self.name = name
        self.texture = texture
        self.x = x
        self.y = y
        self.time = 0
        self.Ttime = Ttime
        App.o.append(self)
        tempMisc.o.append(self)
        
    def remove(self):
        App.o.remove(self)
        tempMisc.o.remove(self)
        
def up(prsn):
    if prsn.y > 0: #up
        if finMap[prsn.x][prsn.y-1] is not wall:
            prsn.y-=1
            time.sleep(0.1)
           
def down(prsn):
    if prsn.y < modeOne.MAPHEIGHT - 1: #down
        if finMap[prsn.x][prsn.y+1] is not wall:
            prsn.y+=1
            time.sleep(0.1)
           
def left(prsn):
    if prsn.x > 0: #LEFT
        if finMap[prsn.x-1][prsn.y] is not wall:
            prsn.x-=1
            time.sleep(0.1)
        
def right(prsn):
    if prsn.x < modeOne.MAPWIDTH - 1: #right
        if finMap[prsn.x+1][prsn.y] is not wall:
            prsn.x+=1
            time.sleep(0.1)
        
def events():
    keys = pygame.key.get_pressed()
    for obj in tempMisc.o:
        if obj.time == obj.Ttime:
            obj.remove()
        else:
            obj.time +=1
    for enemy in Enemy.hit_list:
        for obj in tempMisc.o:
            if enemy.x == obj.x and enemy.y == obj.y:
                enemy.remove()
                PLAYER.score += enemy.adscore
                print(PLAYER.score)
    for player in Player.hit_list:
        if player.immune == True:
            player.imcount +=1
            if player.imcount == player.imtime:
                player.immune = False
                player.imcount = 0
        for enemy in Enemy.hit_list:
            if player.x == enemy.x and player.y == enemy.y and player.immune == False:
                Player.damage()
    for event in pygame.event.get():
        if event.type == QUIT or keys[pygame.K_q]:
            target = open("high_score.txt", "w")
            target.write(str(PLAYER.score))
            print("Quit")
            pygame.quit()
            sys.exit()
            
        if (keys[K_s]):
            PLAYER.texture = downimg
            down(PLAYER)
            PLAYER.tcount +=1
        elif (keys[K_w]):
            PLAYER.texture = upimg
            up(PLAYER)
            PLAYER.tcount +=1
        elif (keys[K_d]):
            PLAYER.texture =rightimg
            right(PLAYER)
            PLAYER.tcount +=1
        elif (keys[K_a]):
            PLAYER.texture =leftimg
            left(PLAYER)
            PLAYER.tcount +=1
            
        if (keys[K_DOWN]):
            PLAYER.tcount +=1
            PLAYER.texture = downimg
            tempMisc("BulletS", pygame.image.load("textures/misc/bulletS.png"),PLAYER.x, PLAYER.y+1)
        elif (keys[K_UP]):
            PLAYER.tcount +=1
            PLAYER.texture = upimg
            tempMisc("BulletW", pygame.image.load("textures/misc/bulletW.png"),PLAYER.x, PLAYER.y-1)
        elif (keys[K_RIGHT]):
            PLAYER.tcount +=1
            PLAYER.texture = rightimg
            tempMisc("BulletD", pygame.image.load("textures/misc/bullet.png"),PLAYER.x+1 , PLAYER.y)
        elif (keys[K_LEFT]):
            PLAYER.tcount +=1
            PLAYER.texture = leftimg
            tempMisc("BulletA", pygame.image.load("textures/misc/bulletA.png"),PLAYER.x-1 , PLAYER.y)
        
        if PLAYER.tcount == ENEMY.tstart:
            Enemy.movement()
            PLAYER.tcount = 0
    
          
          
          
def start():
    DISPLAYSURF=pygame.display.set_mode((modeOne.MAPWIDTH*modeOne.TILESIZE, modeOne.MAPHEIGHT*modeOne.TILESIZE)) 
    Mapgen.mapGen()
    Player.spawn(PLAYER)
    while True: 
        """
        Renders and Draws everything
        Updates movement and anything else that changes during runtime
        """
        events()
        pygame.display.set_caption('CellMata')
        pygame.display.set_icon(pygame.image.load("textures/logo.png"))
        for rw in range(modeOne.MAPWIDTH):
            for cl in range(modeOne.MAPHEIGHT):            
                DISPLAYSURF.blit(Tile.t[finMap[rw][cl].ID].texture, (rw*modeOne.TILESIZE,cl*modeOne.TILESIZE))
                for obj in App.o:
                    DISPLAYSURF.blit(obj.texture,(obj.x*modeOne.TILESIZE, obj.y*modeOne.TILESIZE))
        pygame.display.update()
        Clock.tick(30)
start()