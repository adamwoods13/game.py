# -*- coding: utf-8 -*-
"""
Created on Tue Dec 15 09:04:02 2015

@author: 10woodsa
"""
import cmath
import colorsys
from itertools import chain, combinations, product
import math
import os
import pygame
from random import randrange
import sys

# point difference
def diff(a, b):
	return [b[i] - a[i] for i in range(len(a))]

# dot product
def dot(a, b):
	return sum(a[i] * b[i] for i in range(len(a)))

# matrix intersection
def seg_intersect(a, b):
	da = diff(*a)
	db = diff(*b)
	# da_cc := (-y, x) rotate counter-clockwise
	da_cc = [-da[1], da[0]]
	denom = dot(da_cc, db)
	if not denom:
		return False
	cos = dot(da_cc, diff(b[0], a[0])) / denom
	return tuple(b[0][i] + cos * db[i] for i in (0, 1))

def maxi(idx, itb):
	return max(x[idx] for x in itb)

def mini(idx, itb):
	return min(x[idx] for x in itb)

# geometric range-check
def in_range(a, btm, top, eq=True):
	if btm > top:
		tmp = top
		top = btm
		btm = tmp
	if not eq:
		return a > btm and a < top
	return a >= btm and a <= top

def is_crossing(a, b):
	if any(x in b for x in a) or any(x in a for x in b) or \
	  any(maxi(j, a) < mini(j, b) or mini(j, a) > maxi(j, b) for j in (0, 1)):
		return False
	x = seg_intersect(a, b)
	if x is not False and all(in_range(x[i], max(mini(i, a), mini(i, b)),
	  min(maxi(i, a), maxi(i, b))) for i in (0, 1)):
		return x
	return False

def dist(a, b):
	delta = diff(a, b)
	return (delta[0] ** 2 + delta[1] ** 2) ** .5

def draw_point(surf, coord, c="gray", font=None, text=None):
	c = pygame.Color(c)
	if not surf.get_rect().collidepoint(coord):
		return
	if text is not None and font is not None:
		fs = font.render(text, False, c)
		if coord[0] < 4 + fs.get_width():
			surf.blit(fs, (coord[0] + 4, coord[1] - 6))
		else:
			surf.blit(fs, (coord[0] - 4 - fs.get_width(), coord[1] - 6))
	pygame.draw.circle(surf, c, [int(x) for x in coord], 4)

def slope(dx, dy):
	if dx == 0:
		return "vertical"
	return dy / dx

def shrink(a, b):
	p, q = cmath.polar(complex(*diff(a, b)))
	p -= 1
	return a[0] + p * math.cos(q), a[1] + p * math.sin(q)	

def huecolor(h, v=1):
	return tuple(int(x * 255) for x in colorsys.hsv_to_rgb(h, 1, v))

def polypair(p):
	last = None
	for x in p:
		if last is not None:
			yield (x, last)
		last = x
	yield (x, p[0])


class voronoi:
	point = []
	triang_lines = []
	perp_lines = set()
	pt = 0

	@classmethod
	def randompoints(cls, num, area):
		while len(cls.point) < num:
			p = [randrange(1, area[0] - 1), randrange(1, area[1] - 1)]
			if p not in cls.point:
				cls.point.append(p)

	@classmethod
	def get_line(self, a, b):
		return tuple(self.point[x] for x in (a, b))

	@classmethod
	def maketriangs(cls, res):
		cls.triang_lines = [voronoi(x[0], x[1], res)
		  for x in combinations(range(len(cls.point)), 2)]
		for pt in range(len(cls.point)):
			conns = []
			for l in sorted(cls.triang_lines, key=lambda l: l.dist()):
				if pt in l.p:
					conns.append(l)
			cls.point[pt].append(conns)

	@classmethod
	def drawpoints(cls, surf, font):
		for idx, p in enumerate(cls.point):
			draw_point(surf, p[:2], "gray", font, "{}".format(idx))

	@classmethod
	def calc(cls, surf):
		pt = cls.point[cls.pt]
		conns = pt[2]
		sz = surf.get_size()
		perps = [
			(0, ((0, 0), (sz[0] - 1, 0))),
			(1, ((0, 0), (0, sz[1] - 1))),
			(2, ((sz[0] - 1, 0), tuple(x - 1 for x in sz))),
			(3, ((0, sz[1] - 1), tuple(x - 1 for x in sz))),
		]
		perps.extend((idx + 4, c.perp) for idx, c in enumerate(conns))
		crossings = {}
		for perp, qurp in product(perps, perps):
			if perp == qurp:
				continue
			i = is_crossing(perp[1], qurp[1])
			if i is not False:
				crossings[(perp[0], qurp[0])] = i
		poly = set()
		for ids, c in crossings.items():
			found = False
			for churp in perps:
				if churp[0] in ids:
					continue
				cs = is_crossing((c, pt), churp[1])
				if cs is not False:
					found = True
					break
			if not found:
				poly.add(c)
		# add corner dots deliberately
		for dot in set(chain(*[x[1] for x in perps[:3]])):
			found = False
			for churp in perps:
				cs = is_crossing((dot, pt), churp[1])
				if cs is not False:
					found = True
					break
			if not found:
				poly.add(dot)
		c = huecolor((1 - 1 / (math.sqrt(5 / 2))) * cls.pt, .6)
		poly = sorted(poly,
		  key=lambda l: cmath.polar(complex(*diff(pt[:2], l)))[1])
		for x in polypair(poly):
			k = tuple(sorted(x))
			if k not in cls.perp_lines and (k[1], k[0]) not in cls.perp_lines:
				if any((x[0][i] == 0 and x[1][i] == 0) or
				       any(x[0][i] == sz[j] - 1 and x[1][i] == sz[j] - 1
				       for j in (0, 1)) for i in (0, 1)):
					continue
				cls.perp_lines.add(x)
		if len(poly) > 2:
		# cmath.polar(complex(*diff(a, b)))[1]
			pygame.draw.polygon(surf, c, poly)
		elif len(poly) == 2:
			pygame.draw.line(surf, c, *poly)


	def __init__(self, a, b, res):
		self.p = (a, b)
		self.upd(res)

	def upd(self, res):
		l = self.line()
		self.delta = diff(*l)
		c = tuple(l[0][i] + x / 2 for i, x in enumerate(self.delta))
		self.cperp = (c, (c[0] - self.delta[1], c[1] + self.delta[0]))
		self.perp = self.get_perp(res)

	def line(self):
		return self.get_line(*self.p)

	def dist(self):
		return sum(x ** 2 for x in self.delta) ** .5

	def get_perp(self, res):
		l = self.line()
		v = (l[0], (l[0][0] - self.delta[1], l[0][1] + self.delta[0]))
		sl = slope(-self.delta[1], self.delta[0])
		if sl == "vertical":
			return ((self.cperp[0][0], 0), (self.cperp[0][0], res[1] - 1))
		off = self.cperp[0][1] - self.cperp[0][0] * sl
		if sl < 1:
			return ((0, off), (res[0], off + sl * res[0]));
		return ((-off / sl, 0), ((res[1] - off) / sl, res[1]))

	@classmethod
	def setup(cls, surf, pts=None):
		cls.point.clear()
		cls.perp_lines.clear()
		sz = surf.get_size()
		if pts is None:
			cls.randompoints(30, sz)
		else:
			cls.point = pts
		cls.pt = 0
		cls.maketriangs(sz)
		surf.fill(pygame.Color("white"))

	@classmethod
	def draw(cls, surf, font):
		if cls.pt < len(cls.point):
			voronoi.calc(surf)
		elif cls.pt == len(cls.point):
			sz = surf.get_size()
			for l in cls.perp_lines:
				pygame.draw.line(surf, pygame.Color("white"), l[0], l[1], 3)
			voronoi.drawpoints(surf, font)
		if cls.pt <= len(cls.point):
			cls.pt += 1
			pygame.display.update()

datafile = "voronoi.data"

def main():
	surf = pygame.display.set_mode((800, 600))
	pygame.font.init()
	font = pygame.font.Font(pygame.font.get_default_font(), 20)
	point = None
	if "--stored" in sys.argv and os.access(datafile, os.R_OK):
		point = []
		with open(datafile, "r") as fh:
			point = [
			  [int(y) for y in x.split("/")] for x in fh.read().split(",")]
	voronoi.setup(surf, point)
	voronoi.draw(surf, font)
	running = True
	while running:
		for ev in pygame.event.get():
			if ev.type == pygame.QUIT:
				running = False
			elif ev.type == pygame.KEYDOWN:
				if ev.key in (pygame.K_ESCAPE, pygame.K_q):
					running = False
				elif ev.key == pygame.K_SPACE:
					voronoi.setup(surf)
		if voronoi.pt < len(voronoi.point) + 1:
			voronoi.draw(surf, font)
	if "--stored" in sys.argv:
		with open(datafile, "w") as fh:
			fh.write(",".join("{}/{}".format(*x) for x in voronoi.point))

if __name__ == "__main__":
	main()
