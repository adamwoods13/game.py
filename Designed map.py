import pygame
pygame.init()

fps = 60
playerX = 1
playerY = 1

grass = 0
wall = 1

tileSize = 50
mapWidth = 5
mapHeight = 6
displayWidth = mapWidth*tileSize
displayHeight = mapHeight*tileSize
tileMap = [
            [grass, grass, wall, wall, wall],
            [wall, grass, grass, grass, wall],
            [wall, grass, grass, grass, wall],
            [wall, wall, wall, grass ,wall],
            [wall, grass, grass, grass, wall],
            [wall, wall, wall, wall, wall,]
            ]

textures = {
            grass : pygame.image.load('textures/groundmodel.png'),
            wall : pygame.image.load('textures/wallmodel.png')

            }

player = pygame.image.load("textures/Player/CirclemodelDown.png")

surface = pygame.display.set_mode((displayWidth, displayHeight))
clock = pygame.time.Clock()
gameIsRunning = True

def gameQuit():
    pygame.quit()
    quit()

while gameIsRunning == True:


    for row in range(mapHeight):
        for column in range(mapWidth):
            surface.blit(textures[tileMap[row][column]], (column*tileSize, row*tileSize, tileSize, tileSize))

    #Do collisions for wall in tilemap

    surface.blit(player, (playerX, playerY))

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            gameQuit()

    keysPressed = pygame.key.get_pressed()

    if keysPressed[pygame.K_LEFT] and playerX > 0:
        if tileMap[playerY][playerX - 1] is not wall:
            playerX -= 1
    elif keysPressed[pygame.K_RIGHT] and playerX < mapWidth - 1:
        if tileMap[playerY][playerX + 1] is not wall:
            playerX += 1
    elif keysPressed[pygame.K_UP] and playerY > 0:
        if tileMap[playerY - 1][playerX] is not wall:
            playerY -= 1
    elif keysPressed[pygame.K_DOWN] and playerY < mapHeight - 1:
        if tileMap[playerY + 1][playerX] is not wall:
            playerY += 1

    pygame.display.update()
    clock.tick(fps)
