# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import sys
import pygame
from pygame.locals import *
import random #imports needed modules
import time
from PyQt4 import QtGui

class App: #creates App class
    """
    Starts the Game window.
    Handles running and window settings
    """
    def __init__(self):
        """
        Size of window
        running set to True to it runs
        no display surf
        """
        self._running=True
        self._display_surf=None
        self.size=self.weight, self.height = 640, 400   
    def on_init(self):
        """
        initiate pygame
        Running True
        sets surface size
        """
        pygame.init()
        self._display_surf=pygame.display.set_mode(self.size, pygame.HWSURFACE | pygame.DOUBLEBUF)
        self._running=True
        
class Tile(pygame.Surface):
    """
    sets tiles for surface of game map
    """
    _id=0 #sets ID to start at 0
    t=[] #creates a list for the tiles
    def __init__(self, collide, name, texture):
        """
        Sets behavior of tiles:
        texture
        Collision or not
        name
        can be build upon
        """
        pygame.Surface.__init__(self,(65,65))
        self.ID =Tile._id #sets ID to tiles ID
        Tile._id+=1 #tile ID
        self.name=name
        self.collide=collide
        self.texture=texture
        self.rect=self.texture.get_rect()
        Tile.t.append(self)
        
ground=Tile(False,"Ground",pygame.image.load('textures/tiles/ground.png'))
wall=Tile(True,"Wall",pygame.image.load('textures/tiles/wall.png'))

        
score=142

TILESIZE=66
MAPWIDTH=7
MAPHEIGHT=7
tilemap=[[ground for h in range(MAPWIDTH)]for w in range(MAPHEIGHT)]

def start():
    DISPLAYSURF=pygame.display.set_mode((MAPWIDTH*TILESIZE, MAPHEIGHT*TILESIZE)) 
    mapGen()
    while True: 
        """
        Renders and Draws everything
        Updates movement and anything else that changes during runtime
        """
        pygame.display.set_caption('Insert name here')
        pygame.display.set_icon(pygame.image.load("textures/logo.png"))
        for cl in range(MAPWIDTH):
            for rw in range(MAPHEIGHT):            
                DISPLAYSURF.blit(Tile.t[tilemap[cl][rw].ID].texture, (rw*TILESIZE,cl*TILESIZE))
        pygame.display.update()
start()


